# This should be a test or example startup script
require hmc804x

# HMC804x

epicsEnvSet("ASYN_PORT", "HMC804x-asyn-port")
epicsEnvSet("DEVICE_IP", "127.0.0.1")
epicsEnvSet("P", "FOO:")
epicsEnvSet("R", "BAR:")
drvAsynIPPortConfigure("${ASYN_PORT}", "${DEVICE_IP}:5025")

## Load record instances
dbLoadRecords("$(hmc804x_DB)/hmc804x.template", "PORT=${ASYN_PORT},P=${P},R=${R=}")
dbLoadRecords("$(hmc804x_DB)/channel.template", "PORT=${ASYN_PORT},P=${P},R=${R=},CH=1")
dbLoadRecords("$(hmc804x_DB)/channel.template", "PORT=${ASYN_PORT},P=${P},R=${R=},CH=2")
dbLoadRecords("$(hmc804x_DB)/channel.template", "PORT=${ASYN_PORT},P=${P},R=${R=},CH=3")

iocInit()
